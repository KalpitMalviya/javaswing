import javax.swing.*;
import java.awt.event.*;

public class Swing extends JFrame{

	JLabel  l1, l2, l3, l4;
	JTextField t1, t2;
	JButton b1;

	public Swing() {} 
	public Swing(String s){super (s);}

	public void setComponents(){
		l1 = new JLabel("Enter Two Numbers: ");
		l2 = new JLabel("Enter First Number");
		l3 = new JLabel("Enter Second Number");
		l4 = new JLabel();
		t1 = new JTextField();
		t2 = new JTextField();
		b1 = new JButton("Add");

		setLayout(null);

		add(l1);
		add(l2);
		add(t1);  
		add(l3);  
		add(t2); 
		add(b1);
		add(l4);

		b1.addActionListener(new Handler());
		
		// set the positions of layout
		l1.setBounds(100,50,500, 30);
		l2.setBounds(50,80,200, 30);
		t1.setBounds(200,80,200, 30);
		l3.setBounds(50,150,200, 30);
		t2.setBounds(200,150,200, 30);
		b1.setBounds(50,200,100, 30);
		l4.setBounds(200,200,100, 30);
		
	}

	public class Handler implements ActionListener{

		public void actionPerformed(ActionEvent e){
			int a = Integer.parseInt(t1.getText());
			int b = Integer.parseInt(t2.getText());
			int c = a+b;
			l4.setText("Sum is : "+c);
		}

	}

	public static void main(String[] args) {
		
		Swing jf = new Swing("Java Swing");
		jf.setComponents();


		jf.setVisible(true); 
		jf.setSize(500,500);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}
